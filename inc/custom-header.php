<?php
/**
 * @package College Web Starter
 */

/**
 * Set up the WordPress core custom header feature.
 *
 * @uses college_web_starter_header_style()
 * @uses college_web_starter_admin_header_style()
 * @uses college_web_starter_admin_header_image()
 */
function college_web_starter_custom_header_setup() {
	add_theme_support( 'custom-header', apply_filters( 'college_web_starter_custom_header_args', array(
		'default-image'          => '',
		'default-text-color'     => 'black',
		'width'                  => 960,
		'height'                 => 110,
		'flex-height'            => false,
		'wp-head-callback'       => 'college_web_starter_header_style',
		'admin-head-callback'    => 'college_web_starter_admin_header_style',
		'admin-preview-callback' => 'college_web_starter_admin_header_image',
	) ) );

	// Default custom headers packaged with the theme. %s is a placeholder for the theme template directory URI.
	// To reference the child theme's directory instead, use %2$s instead of %s below.
		register_default_headers( array(
			'greenback' => array(
				'url' => '%s/images/headers/CollegeHeaderGreen.jpg',
				'thumbnail_url' => '%s/images/headers/CollegeHeaderGreen-thumbnail.jpg',
				/* translators: header image description */
				'description' => __( 'WF College logo header image with white to green gradient background.', 'college-web-starter' )
			),
			'bluegradient' => array(
				'url' => '%s/images/headers/CollegeHeaderGrBlue.jpg',
				'thumbnail_url' => '%s/images/headers/CollegeHeaderGrBlue-thumbnail.jpg',
				/* translators: header image description */
				'description' => __( 'WF College logo header image with white to blue gradient background.', 'college-web-starter' )
			),
			'dkgrayback' => array(
				'url' => '%s/images/headers/CollegeHeaderGrDarkWhtText.jpg',
				'thumbnail_url' => '%s/images/headers/CollegeHeaderGrDarkWhtText-thumbnail.jpg',
				/* translators: header image description */
				'description' => __( 'WF College logo header image with white to dark gray gradient background.', 'college-web-starter' )
			),
			'ltgraytofuchsiaback' => array(
				'url' => '%s/images/headers/CollegeHeaderGrFusia.jpg',
				'thumbnail_url' => '%s/images/headers/CollegeHeaderGrFusia-thumbnail.jpg',
				/* translators: header image description */
				'description' => __( 'WF College logo header image with light gray to fuchsia gradient background.', 'college-web-starter' )
			),
			'goldgradientback' => array(
				'url' => '%s/images/headers/CollegeHeaderGrGold.jpg',
				'thumbnail_url' => '%s/images/headers/CollegeHeaderGrGold-thumbnail.jpg',
				/* translators: header image description */
				'description' => __( 'WF College logo image with light gray to gold gradient background', 'college-web-starter' )
			),
			'allwhite' => array(
				'url' => '%s/images/headers/CollegeHeaderAllWhite.jpg',
				'thumbnail_url' => '%s/images/headers/CollegeHeaderAllWhite-thumbnail.jpg',
				/* translators: header image description */
				'description' => __( 'WF College logo header image with all white background.', 'college-web-starter' )
			),
			'gold' => array(
				'url' => '%s/images/headers/CollegeHeaderGold.jpg',
				'thumbnail_url' => '%s/images/headers/CollegeHeaderGold-thumbnail.jpg',
				/* translators: header image description */
				'description' => __( 'WF College logo header image with logo in white on a solid gold background.', 'college-web-starter' )
			),
			'black' => array(
				'url' => '%s/images/headers/CollegeHeaderBlk.jpg',
				'thumbnail_url' => '%s/images/headers/CollegeHeaderBlk-thumbnail.jpg',
				/* translators: header image description */
				'description' => __( 'WF College logo header image with black background.', 'college-web-starter' )
			),
			'blue' => array(
				'url' => '%s/images/headers/CollegeHeaderBlueSky.jpg',
				'thumbnail_url' => '%s/images/headers/CollegeHeaderBlueSky-thumbnail.jpg',
				/* translators: header image description */
				'description' => __( 'WF College logo header image with dark blue background.', 'college-web-starter' )
			),
			'blkbox' => array(
				'url' => '%s/images/headers/CollegeHeaderBlkBox.jpg',
				'thumbnail_url' => '%s/images/headers/CollegeHeaderBlkBox-thumbnail.jpg',
				/* translators: header image description */
				'description' => __( 'WF College logo header image with logo in white in a black box to the left.', 'college-web-starter' )
			),
			'goldbox' => array(
				'url' => '%s/images/headers/CollegeHeaderGoldBox.jpg',
				'thumbnail_url' => '%s/images/headers/CollegeHeaderGoldBox-thumbnail.jpg',
				/* translators: header image description */
				'description' => __( 'WF College logo header image with logo in white in a gold box to the left.', 'college-web-starter' )
			),
		) );
/* end of default custom header stuff */
} /* end wfcolllege_two_custom_header_setup function. */

add_action( 'after_setup_theme', 'college_web_starter_custom_header_setup' );

if ( ! function_exists( 'college_web_starter_header_style' ) ) :
/**
 * Styles the header image and text displayed on the blog
 *
 * @see college_web_starter_custom_header_setup().
 */
function college_web_starter_header_style() {
	$header_text_color = get_header_textcolor();

	// If no custom options for text are set, let's bail
	// get_header_textcolor() options: HEADER_TEXTCOLOR is default, hide text (returns 'blank') or any hex value
	if ( HEADER_TEXTCOLOR == $header_text_color ) {
		return;
	}

	// If we get this far, we have custom styles. Let's do this.
	?>
	<style type="text/css">
	<?php
		// Has the text been hidden?
		if ( 'blank' == $header_text_color ) :
	?>
		.site-branding {
			display: none;
		}
	<?php
		// If the user has set a custom color for the text use that
		else :
	?>
		.site-title a,
		.site-description {
			color: #<?php echo esc_attr( $header_text_color ); ?>;
		}
	<?php endif; ?>
	</style>
	<?php
}
endif; // college_web_starter_header_style

if ( ! function_exists( 'college_web_starter_admin_header_style' ) ) :
/**
 * Styles the header image displayed on the Appearance > Header admin panel.
 *
 * @see college_web_starter_custom_header_setup().
 */
function college_web_starter_admin_header_style() {
?>
	<style type="text/css">
		.appearance_page_custom-header #headimg {
			border: none;
		}
		#headimg div,
		#desc {
		}
		#headimg div {
		}
		#headimg div a {
		}
		#desc {
		}
		#headimg img {
		}
	</style>
<?php
}
endif; // college_web_starter_admin_header_style

if ( ! function_exists( 'college_web_starter_admin_header_image' ) ) :
/**
 * Custom header image markup displayed on the Appearance > Header admin panel.
 *
 * @see college_web_starter_custom_header_setup().
 */
function college_web_starter_admin_header_image() {
	$style = sprintf( ' style="color:#%s;"', get_header_textcolor() );
?>
	<div id="headimg">
		<div class="displaying-header-text"><a id="name"<?php echo $style; ?> onclick="return false;" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a></div>
		<div class="displaying-header-text" id="desc"<?php echo $style; ?>><?php bloginfo( 'description' ); ?></div>
		<?php if ( get_header_image() ) : ?>
		<img src="<?php header_image(); ?>" alt="">
		<?php endif; ?>
	</div>
<?php
}
endif; // college_web_starter_admin_header_image