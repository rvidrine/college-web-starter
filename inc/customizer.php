<?php
/**
 * College Web Starter Theme Customizer
 *
 * @package College Web Starter
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function college_web_starter_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
// Create toggle for header login link
	$wp_customize->add_setting( 'header_loginout_link', array(
		'type'			=>	'theme_mod',
		'default'		=>	'On',
		'sanitize_callback'	=>	'',
		'transport'	=>	'refresh'
	) );	
	$wp_customize->add_section( 'header_links', array(
		'title'	=> __( 'Header Links', 'college-web-starter'),
		'priority'	=> 5
	) );
// Create a control for header loginout link on and off
	$wp_customize->add_control( 'header_loginout_link', array(
		'type'			=>	'radio',
		'choices'	=>	array(
			'header_loginout_on'	=> 'On',
			'header_loginout_off'		=>	'Off'
			),
		'label'        => __( 'Turn Header Login/Logout link on or off', 'college-web-starter' ),
		'section'    => 'header_links',
		'setting'   => 'header_loginout_link',
	) );
	// Create the color scheme setting
	$wp_customize->add_setting( 'color_scheme', array(
		'type'			=>	'theme_mod',
		'default'		=>	'plain',
		'sanitize_callback'	=>	'sanitize_html_class',
		'transport'	=>	'refresh'
	) );	
// Create a control for changing the color scheme
	$wp_customize->add_control( 'color_scheme', array(
		'type'			=>	'radio',
		'choices'	=>	array(
			'default'	=> 'Basic White',
			'style-one'		=>	'Style 1',
			'style-two'		=>	'Style 2',
			'style-three'		=>	'Style 3',
			'style-four'		=>	'Style 4',
			'style-five'		=>	'Style 5'
			),
		'label'        => __( 'Select Color Scheme', 'college-web-starter' ),
		'section'    => 'colors',
		'setting'   => 'color_scheme',
	) );
	$wp_customize->add_setting( 'dept_address_line_one', array(
		'type'			=>	'theme_mod',
		'default'		=>	'1834 Wake Forest Road',
		'sanitize_callback'	=>	'sanitize_text_field',
		'transport'	=>	'refresh'
	) );	
	$wp_customize->add_setting( 'dept_office_location', array(
		'type'			=>	'theme_mod',
		'default'		=>	'Building and Room Number',
		'sanitize_callback'	=>	'sanitize_text_field',
		'transport'	=>	'refresh'
	) );	
	$wp_customize->add_setting( 'dept_po_box', array(
		'type'			=>	'theme_mod',
		'default'		=>	'1234',
		'sanitize_callback'	=>	'sanitize_text_field',
		'transport'	=>	'refresh'
	) );	
	$wp_customize->add_setting( 'dept_phone_extension', array(
		'type'			=>	'theme_mod',
		'default'		=>	'1234',
		'sanitize_callback'	=>	'sanitize_text_field',
		'transport'	=>	'refresh'
	) );	
	$wp_customize->add_setting( 'dept_fax_extension', array(
		'type'			=>	'theme_mod',
		'default'		=>	'1234',
		'sanitize_callback'	=>	'sanitize_text_field',
		'transport'	=>	'refresh'
	) );	
	$wp_customize->add_setting( 'dept_email', array(
		'type'			=>	'theme_mod',
		'default'		=>	'DEPTNAME@wfu.edu',
		'sanitize_callback'	=>	'sanitize_email',
		'transport'	=>	'refresh'
	) );
	$wp_customize->add_section( 'dept_info', array(
		'title'	=> __( 'Department Contact Information', 'college-web-starter'),
		'priority'	=> 25
	) );
	$wp_customize->add_control( 'dept_address_line_one', array(
		'label'        => __( 'Mailing Address Line 1', 'college-web-starter' ),
		'section'    => 'dept_info',
		'setting'   => 'dept_address_line_one',
	) );
	$wp_customize->add_control( 'dept_office_location', array(
		'label'        => __( 'Main Office Location', 'college-web-starter' ),
		'section'    => 'dept_info',
		'setting'   => 'dept_office_location',
	) );
	$wp_customize->add_control( 'dept_po_box', array(
		'type'	=> 'number',
		'label'        => __( 'Department PO Box Number', 'college-web-starter' ),
		'section'    => 'dept_info',
		'setting'   => 'dept_po_box',
	) );
	$wp_customize->add_control( 'dept_phone_extension', array(
		'type'	=> 'number',
		'label'        => __( 'Main Office Phone Extension', 'college-web-starter' ),
		'section'    => 'dept_info',
		'setting'   => 'dept_phone_extension',
	) );
	$wp_customize->add_control( 'dept_fax_extension', array(
		'type'	=> 'number',
		'label'        => __( 'Main Office Fax Extension', 'college-web-starter' ),
		'section'    => 'dept_info',
		'setting'   => 'dept_fax_extension',
	) );
	$wp_customize->add_control( 'dept_email', array(
		'type'	=> 'email',
		'label'        => __( 'Department Email Address', 'college-web-starter' ),
		'section'    => 'dept_info',
		'setting'   => 'dept_email',
	) );

	}
add_action( 'customize_register', 'college_web_starter_customize_register' );

/* Output the value of the theme_mod color-scheme to then put it into the body classes */
function college_web_starter_colorscheme_body_class( $classes ) {
	$colorscheme = get_theme_mod( 'color_scheme' );
	$classes[] = 'color-scheme-' . $colorscheme;
	return $classes;
}
/* Put the correct color scheme class into the body classes */
add_filter( 'body_class', 'college_web_starter_colorscheme_body_class' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function college_web_starter_customize_preview_js() {
	wp_enqueue_script( 'college_web_starter_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'college_web_starter_customize_preview_js' );
