<?php

require_once dirname(__FILE__) . '/cws-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'cws_register_required_plugins' );


function cws_register_required_plugins() {
	
	$plugins = array(
	
		array(
				'name'        => 'Tabby Responsive Tabs',
				'slug'        => 'tabby-responsive-tabs',
				'required'	  => true,
				'is_callable' => 'tabby_init',
				'force_activation' => false,
			),
		array(
				'name'        => 'WP User Avatar',
				'slug'        => 'wp-user-avatar',
				'required'	  => true,
				'is_callable' => 'wp_user_avatar_init',
				'force_activation' => false,
			),	
		array(
				'name'        => 'Breadcrumb Trail',
				'slug'        => 'breadcrumb-trail',
				'is_callable' => 'breadcrumb-trail_init',
				'force_activation' => false,
			),
		array(
				'name'        => 'Category Posts Widget',
				'slug'        => 'category-posts',
				'force_activation' => false,
			),	
		array(
				'name'        => 'Enable Media Replace',
				'slug'        => 'enable-media-replace',
				'is_callable' => 'enable-media-replace_init',
				'force_activation' => false,
			),	
		array(
				'name'        => 'Meteor Slides',
				'slug'        => 'meteor-slides',
				'required'	  => true,
				'is_callable' => 'meteor-slides_init',
				'force_activation' => false,
			),	
		array(
				'name'        => 'Simple Calendar',
				'slug'        => 'google-calendar-events',
				'is_callable' => 'google-calendar-events_init',
				'force_activation' => false,
			),	
		array(
				'name'        => 'TinyMCE Advanced',
				'slug'        => 'tinymce-advanced',
				'required'	  => true,
				'is_callable' => 'tinymce-advanced_init',
				'force_activation' => false,
			),	
		array(
				'name'        => 'Recent Posts Widget with Thumbnails',
				'slug'        => 'recent-posts-widget-with-thumbnails',
			),
		array(
				'name'        => 'Github Updater',
				'slug'        => 'github-updater',
				'required'	  => true,
				'source'	  => get_template_directory_uri() . '/inc/plugins/github-updater.zip',
			),
	);
		$config = array(
		'id'           => 'cws_tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'plugins.php',            // Parent menu slug.
		'capability'   => 'install_plugins',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => false,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => true,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.

		
		'strings'      => array(
			'page_title'                      => __( 'Plugins Recommended with College Web Starter', 'college-web-starter' ),
			'menu_title'                      => __( 'Install College Web Starter Plugins', 'college-web-starter' ),
			'installing'                      => __( 'Installing Plugin: %s', 'college-web-starter' ), // %s = plugin name.
			'oops'                            => __( 'Something went wrong with the plugin API.', 'college-web-starter' ),
			'notice_can_install_required'     => _n_noop(
				'College Web Starter requires the following plugin: %1$s.',
				'College Web Starter requires the following plugins: %1$s.',
				'college-web-starter'
			), // %1$s = plugin name(s).
			'notice_can_install_recommended'  => _n_noop(
				'College Web Starter recommends the following plugin: %1$s.',
				'College Web Starter recommends the following plugins: %1$s.',
				'college-web-starter'
			), // %1$s = plugin name(s).
			'notice_cannot_install'           => _n_noop(
				'Sorry, but you do not have the correct permissions to install the %1$s plugin.',
				'Sorry, but you do not have the correct permissions to install the %1$s plugins.',
				'college-web-starter'
			), // %1$s = plugin name(s).
			'notice_ask_to_update'            => _n_noop(
				'The following plugin needs to be updated to its latest version to ensure maximum compatibility with College Web Starter: %1$s.',
				'The following plugins need to be updated to their latest version to ensure maximum compatibility with College Web Starter: %1$s.',
				'college-web-starter'
			), // %1$s = plugin name(s).
			'notice_ask_to_update_maybe'      => _n_noop(
				'There is an update available for: %1$s.',
				'There are updates available for the following plugins: %1$s.',
				'college-web-starter'
			), // %1$s = plugin name(s).
			'notice_cannot_update'            => _n_noop(
				'Sorry, but you do not have the correct permissions to update the %1$s plugin.',
				'Sorry, but you do not have the correct permissions to update the %1$s plugins.',
				'college-web-starter'
			), // %1$s = plugin name(s).
			'notice_can_activate_required'    => _n_noop(
				'The following required plugin is currently inactive: %1$s.',
				'The following required plugins are currently inactive: %1$s.',
				'college-web-starter'
			), // %1$s = plugin name(s).
			'notice_can_activate_recommended' => _n_noop(
				'The following recommended plugin is currently inactive: %1$s.',
				'The following recommended plugins are currently inactive: %1$s.',
				'college-web-starter'
			), // %1$s = plugin name(s).
			'notice_cannot_activate'          => _n_noop(
				'Sorry, but you do not have the correct permissions to activate the %1$s plugin.',
				'Sorry, but you do not have the correct permissions to activate the %1$s plugins.',
				'college-web-starter'
			), // %1$s = plugin name(s).
			'install_link'                    => _n_noop(
				'Begin installing plugin',
				'Begin installing plugins',
				'college-web-starter'
			),
			'update_link' 					  => _n_noop(
				'Begin updating plugin',
				'Begin updating plugins',
				'college-web-starter'
			),
			'activate_link'                   => _n_noop(
				'Begin activating plugin',
				'Begin activating plugins',
				'college-web-starter'
			),
			'return'                          => __( 'Return to Required Plugins Installer', 'college-web-starter' ),
			'plugin_activated'                => __( 'Plugin activated successfully.', 'college-web-starter' ),
			'activated_successfully'          => __( 'The following plugin was activated successfully:', 'college-web-starter' ),
			'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'college-web-starter' ),  // %1$s = plugin name(s).
			'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for College Web Starter. Please update the plugin.', 'college-web-starter' ),  // %1$s = plugin name(s).
			'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'college-web-starter' ), // %s = dashboard link.
			'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'college-web-starter' ),

			'nag_type'                        => 'updated', // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
		),
		
	);

	tgmpa( $plugins, $config );
}



?>