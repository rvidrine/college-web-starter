<?php
	
	/* Create Dept Contact Info widget */
	class Dept_Contact_Info_Widget extends WP_Widget {

		/**
		 * Register widget with WordPress.
		 */
		function __construct() {
			parent::__construct(
				'dept_contact_info_widget', // Base ID
				__( 'Department Contact Information', 'college-web-starter' ), // Name
				array( 
					'description' => __( 'Display Department Contact Information from the Customize area', 'college-web-starter' ), 
				) // Args
			);
		}

		/**
		 * Front-end display of widget.
		 *
		 * @see WP_Widget::widget()
		 *
		 * @param array $args     Widget arguments.
		 * @param array $instance Saved values from database.
		 */
		public function widget( $args, $instance ) {
			echo $args['before_widget'];
			if ( ! empty( $instance['title'] ) ) {
				echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
			}
			/* The actual content of the widget, including values from the Customizer. */			
			if ( get_theme_mod( 'dept_address_line_one' ) ) { 
				echo get_theme_mod( 'dept_address_line_one', '1834 Wake Forest Road' ); ?><br /> 
			<?php } ?>
			<?php if ( get_theme_mod( 'dept_office_location' ) ) {
				echo get_theme_mod( 'dept_office_location', 'BUILDING AND ROOM NUMBER' ); ?><br />
			<?php } ?>
			<?php if ( get_theme_mod( 'dept_po_box' ) ) { ?>P.O. Box <?php echo get_theme_mod( 'dept_po_box', '1834' ); ?>,  <?php } ?>Winston-Salem, NC 27109<br />
			<?php if ( get_theme_mod( 'dept_phone_extension' ) ) { ?>
				<i class="fa fa-phone"></i><span class="screen-reader-text">Phone: </span> (336) 758.<?php echo get_theme_mod( 'dept_phone_extension', '1234' ); ?><br />
			<?php } ?>
			<?php if ( get_theme_mod( 'dept_fax_extension' ) ) { ?>
				<i class="fa fa-fax"></i><span class="screen-reader-text">FAX: </span> (336) 758.<?php echo get_theme_mod( 'dept_fax_extension', '1234' ); ?><br />
			<?php } ?>
			<?php if ( get_theme_mod( 'dept_email' ) ) { ?>
				<span class="screen-reader-text">Email: </span> <a href="mailto:<?php echo get_theme_mod( 'dept_email' ); ?>"><?php echo get_theme_mod( 'dept_email', 'DEPTNAME@wfu.edu' ); ?></a>
			<?php } ?>
			<?php
			echo $args['after_widget'];
		}

		/**
		 * Back-end widget form.
		 *
		 * @see WP_Widget::form()
		 *
		 * @param array $instance Previously saved values from database.
		 */
		public function form( $instance ) {
			$title = ! empty( $instance['title'] ) ? $instance['title'] : get_bloginfo( 'title' );
			?>
			<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
			</p>
			<?php 
		}

		/**
		 * Sanitize widget form values as they are saved.
		 *
		 * @see WP_Widget::update()
		 *
		 * @param array $new_instance Values just sent to be saved.
		 * @param array $old_instance Previously saved values from database.
		 *
		 * @return array Updated safe values to be saved.
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

			return $instance;
		}

	} // class Dept_Contact_Info_Widget

	/* Register the widget */
	function cws_load_custom_widgets() {
		register_widget( 'Dept_Contact_Info_Widget' );
	}
	add_action( 'widgets_init', 'cws_load_custom_widgets' );