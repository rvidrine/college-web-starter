<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package College Web Starter
 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
	<?php get_sidebar( 'footer' ); ?>
	<?php if( has_nav_menu( 'footer-social' ) ) {
		college_web_starter_footer_social_menu();
		}
		else {
			get_sidebar( 'footer-social-alternate' );
		} ?>
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://www.wfu.edu/', 'college-web-starter' ) ); ?>"><?php esc_html( printf( __( '&copy; %s Wake Forest University', 'college-web-starter' ), esc_html( current_time( 'Y' ) ) ) ); ?></a>
<!-- lines commented out to remove the theme name and author name
			<span class="sep"> | </span>
			<?php /**
			printf( __( 'Theme: %1$s by %2$s.', 'college-web-starter' ), 'College Web Starter', '<a href="http://college.wfu.edu/itg/robert-vidrine" rel="designer">Robert Vidrine</a>' ); **/
			?>
-->
				
					<?php if (function_exists ( 'the_privacy_policy_link' ) ) { 'the_privacy_policy_link' ( '<div class="privacy-policy-link">' , '</div>' ); } 
					?>
			</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
