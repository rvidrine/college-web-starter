<?php

/* Theme PHP code will go here. */

/**
 * College Web Starter functions and definitions
 *
 * @package College Web Starter
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'college_web_starter_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function college_web_starter_setup() {
	/* Add theme support fo rhe title_tag feature, now required of all themes to
	meet the theme standards.	*/
	add_theme_support( 'title-tag' );
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 */
	load_theme_textdomain( 'college-web-starter', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	/* set the size of the post thumbnails for all featured image locations */
set_post_thumbnail_size (150, 150);

	// This theme uses wp_nav_menu() in three locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'college-web-starter' ),
		'social' => __( 'Social Menu', 'college-web-starter'),
		'footer-social' => __( 'Footer Social Menu', 'college-web-starter'),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	// Set up the WordPress core custom background feature.
/* Hide this section, leaving the color control up to teh color schemes
* Remove this comment (and the end comment tag) to enable custom background
	add_theme_support( 'custom-background', apply_filters( 'college_web_starter_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
End commenting */
	}

endif; // college_web_starter_setup
add_action( 'after_setup_theme', 'college_web_starter_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
 
 function college_web_starter_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Main Sidebar', 'college-web-starter' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Widgets', 'college-web-starter' ),
		'id'            => 'sidebar-footer',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => __( 'Front Page Widgets', 'college-web-starter' ),
		'id'            => 'sidebar-frontpage',
		'description'   => 'Widgets that will only show on the front page of the site when using a Static Front Page.',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
		register_sidebar( array(
		'name'          => __( 'Footer Alternative Social Media Widgets', 'college-web-starter' ),
		'id'            => 'footer-social-alternate',
		'description'   => 'Widget area that is only used if no menu is active in the footer social media area.',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

} // college_web_starter_widgets_init
add_action( 'widgets_init', 'college_web_starter_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function college_web_starter_scripts() {

    wp_enqueue_style( 'college-web-starter-core-style' , get_template_directory_uri() . '/style.css');
	
if (is_page_template('page-nosidebar.php')) {
    wp_enqueue_style( 'college-web-starter-layout-style' , get_template_directory_uri() . '/layouts/nosidebar.css');
} else {
    wp_enqueue_style( 'college-web-starter-layout-style' , get_template_directory_uri() . '/layouts/content-sidebar.css');
}
	wp_enqueue_style( 'college-web-starter-font-awesome', get_template_directory_uri() . '/icons/font-awesome-4.5.0/css/font-awesome.min.css' );
	
		wp_enqueue_script( 'college-web-starter-superfish', get_template_directory_uri() . '/js/superfish.min.js', array('jquery'), '20141113', true );
	

	wp_enqueue_script( 'college-web-starter-superfish-settings', get_template_directory_uri() . '/js/superfish-settings.js', array('college-web-starter-superfish'), '20141113', true );

	wp_enqueue_script( 'college-web-starter-hover-intent', get_template_directory_uri() . '/js/hoverIntent.js', array('college-web-starter-superfish'), '20141113', true );	
	
	wp_enqueue_script( 'college-web-starter-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( 'college-web-starter-hide-search', get_template_directory_uri() . '/js/hide-search.js', array(), '20141117', true );

	wp_enqueue_script( 'college-web-starter-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'college_web_starter_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';


/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Use Thomas Griffin's drop-in for recommended plugins.
 */
require get_template_directory() . '/inc/tgmpa-config.php';

/**
 * Load the Department Contact Info Widget..
 */
require get_template_directory() . '/inc/dept-contact-info-widget.php';
/**
 * Load the Department Contact Info Widget..
 */
require get_template_directory() . '/inc/simple-login-link-widget.php';

/* Remove the ability to "cowboy code" by removing the Editor link under
* Appearance->Editor and the one under Plugins->Editor. If you are
reading this comment and are frustrated by not being able to get to these
* areas, you are obviously not afraid of code, and should just be creating
* a child theme. Contact Robert Vidrine for help creating a child theme. */
function cws_remove_code_editors() {
	remove_submenu_page( 'themes.php', 'theme-editor.php' );
	remove_submenu_page( 'plugins.php', 'plugin-editor.php' );
}
add_action( 'admin_init', 'cws_remove_code_editors' );


/* This function, is_tree checks to see if a page is ANY ancestor of another page. Function is from http://codex.wordpress.org/Conditional_Tags */
  function is_tree($pid) {      // $pid = The ID of the page we're looking for pages underneath
    global $post;         // load details about this page
    $anc = get_post_ancestors( $post->ID );
    foreach($anc as $ancestor) {
      if(is_page() && $ancestor == $pid) {
        return true;
      }
    }
    if(is_page()&&(is_page($pid)))
                 return true;   // we're at the page or at a sub page
    else
                 return false;  // we're elsewhere
  };
