<?php
/**
 * The front page sidebar. This shows below the content area on the front page.
 *
 */

if ( ! is_active_sidebar( 'sidebar-frontpage' ) ) {
	/* Do something if nothing has been added to the front page sidebar. */
}
else { ?>
	<div class="widget-wrapper">
	<div id="frontpage-widgets" class="frontpage-widgets clear" role="complementary">
	<?php
		dynamic_sidebar( 'sidebar-frontpage' );
	?>
	</div> <!-- .frontpage-widgets -->
</div> <!-- .widget-wrapper -->
<?php
};
