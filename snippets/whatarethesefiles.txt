These files are snippets of code that can be placed into a child theme to enable extra functionality.
YOU MUST CREATE A CHILD THEME FOR THESE ADDITIONS.
If you simply change the files in College Web Starter, then YOUR CHANGES WILL BE LOST the first time that CWS updates. The only thing that is needed in a child theme is a style.css that has this sort of header, with any special CSS rules you want listed below it:
/*
* Theme Name: My Child Theme Name
* Template: college-web-starter
*/

The folder that contains your child theme style.css can contain any number of PHP, javascript or other kinds of files. If you include files that are already in the parent theme (College Web Starter), then your child theme's version will be used instead of the parent's. The only exception is the functions.php file. If you have a functions.php file in your child theme, it will be loaded first, then the one in the parent theme will be loaded. Depending on how the function is written, this may mean that you cannot simply call the same function in your child theme's functions.php in order to override the parent theme's version.

Enjoy!