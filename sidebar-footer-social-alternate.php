<?php
/**
 * The footer sidebar
 *
 */

if ( ! is_active_sidebar( 'footer-social-alternate' ) ) {}
else { ?>

<div class="footer-social-alternate-wrapper">
	<div id="footer-social-alternate" class="footer-social-alternate widget-area clear" role="complementary">
		<?php dynamic_sidebar( 'footer-social-alternate' ); ?>
	</div><!-- #footer-sidebar -->
</div><!-- #supplementary -->
<?php } 
