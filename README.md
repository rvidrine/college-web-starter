# Welcome to the College Web Starter theme! #
This theme is designed for use by academic departments and organizations at Wake Forest University. While the theme is licensed under the GPL, so anyone is free to use it, edit it, and create their own works from it (those works must also be licensed under the GPL), there are lots of design and functionality choices that have been made to accomodate only the needs of a very specific audience.

Although the bulk of the programming was done by Robert Vidrine, significant code submissions and consultation from Tyler Pruitt, Tommy Murphy and Jo Lowe were instrumental in the development of this theme. Thanks also Thomas Griffin, for his excellent Plugin Activation code, which provides the functionality for the recommended and required plugin notices. (His code can be found at http://tgmpluginactivation.com .)

## Manual Installation: ##
1. Download the .zip version from the git repository: https://bitbucket.org/rvidrine/college-web-starter.
2. Rename the zip file to college-web-starter.zip.
3. Either upload the theme into your site's wp-content/themes folder and unzip it, or go to Appearance -> Themes, click Add New, and browse to the college-web-starter.zip file on your computer to upload it.
4. Install any suggested plugins as needed. Some standard features, like easy faculty listings, will not be enabled unless certain plugins are installed, but otherwise the theme will work without them.
