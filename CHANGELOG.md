# Changelog: #
### 1.5.2 ###
- Feature: Added Privacy Policy link in footer just below the copyright statement, activated when a privacy policy page is created or selected in the WordPress Settings.
- Bug: Fixed sizing of search field when viewing site from a small screen so that the text box will be large enough to see.
- Bug: Added escaping functions to footer.php to increase security according to best practices in WordPress.
### 1.5.1 ###
- Feature: Removed CollegePeople Tools as a recommended plugin since this plugin is no longer in active development and the author, Tyler Pruitt, is developing a new plugin to handle this functionality.
- Feature: Removed WFU LDAP Authentication plugin from recommended plugins and removed embedded version as authentication on the new WFU web hosting platform will not support LDAP.
- Feature: Removed WordPress Importer from recommended plugins as this plugin is already linked to the import process and no configuration is needed.
- Feature: Updated versino of embedded plugin Github Updater to 8l8.1.
- Bug: Changed the h1 tag of the site title to be a div so there is only one h1 on the page. This should improve accessibility score and make the page more readable.
- Removed force-new-password.php from inc folder, as this was only used when using a backup of a fully-configured site like a site starter. Since there is a much better templating system in Installatron, this functionis no longer needed.
### 1.5 ###
- Bug: Format the README.md with proper Markdown syntax so it will display more attractively on bitbucket and other contexts.
- Feature: Create separate CHANGELOG.md and move the changelog there, so it will display when one is prompted for an update and clicks the link to see the details.
-Feature: Make a Simple Login Link widget which can be added to any widget area.
- Feature: Add Simple Login Link widget to the header of every page. Unlike the Meta widget, it returns a visitor to the page on which they clicked the link after login is complete (instead of the admin area).
-Feature: Set the copyright notice at the bottom of every page to display the current year.
-Feature: Remove Google Analyticator from the recommended plugins list since it has produced difficult to solve errors recently and there are other alternatives that work well. (A new Google Analytics plugin will be added to the recommended plugins in an upcoming version.)
-Bug: Added conditional check so that te .site-description h2 is not created if the tagline is blank.
### 1.4.2 ###
-Bug: Added white background behind breadcrumb trail so that it shows up properly when other than white color scheme is chosen.
-Feature: Removed Google Analyticator from the recommended plugin list, as this plugin has been giving problems recently, and other better plugins exist.
### 1.4.1 ###
- Bug: Correct a typo in the description of the Front Page Widget Area
- Bug: Make posts inserted on the front page using a widget and the Front Page Widget area the same font as the static text on the front page.
### 1.4 ###
- Bug: Make several Department Contact Information fields not display if left blank in the Customizer: Address Line One, Main Department Office Location, Main Department Office Phone Extension, Department Email.
- Bug: Fixed bug in the Department Contact Info widget where the first line of the address couldn't be changed from the default '1834 Wake Forest Rd.'
- Bug: Changed the border between nav menu items from left to right so that there is a border between the last social media icon and the search icon.
- Feature: Created a widget area in the footer social media menu location so that one can put their own widgets in that area if they want colored social media icons. This widget area is only displayed on the front end if there is no menu placed in the "Footer Social Menu" location. (The widget area always appears in the admin area.)
### 1.3 ###
- Feature: Added Front Page widget area, which appears on the front page only, below the static content on that page.
- Feature: Bundled Github Updater with the theme in the TGMPA plugin recommender section.
### 1.2.2 ###
- Bug: Fixed bug when selecting "Latest Posts" for front page. Page now correctly displays posts on front page. This also resulted in the posts not appearing on the Static Front Page when selected.
- Feature: Add Recent Posts Widget with Thumbnails to TGMPA plugin recommendations.
- Bug: Change Dept. Contact Info widget so that PO Box is not displayed if left blank in the Customizer.
- Bug: Removed hard-coded content (posts, etc.) from the 404 page template, so only a search box appears.
### 1.2.1 ###
- Updated the bundled version of College People Tools to 0.6, which includes changes to allow one-click updating (along with the normal WordPress update process). Also changed the way that Font Awesome icon fonts were included, bundling them with the theme instead of calling them from a CDN (which is discouraged by the WordPress theme review team and also gets graded down in Google Page Speed Insights).
### 1.2 ###
- Added 'Department Contact Information' section to the customizer which modifies the default footer content, allowing one to simply input their department or program's contact information (mailing address, office location, phone, fax and email) and have this information appear in a formatted fashion in the footer, instead of having to build this from scratch. If one instead puts one or more widgets into the footer area, the default footer content will disappear and be replaced by their widgets.
### 1.1.6 ###
- Remove the unprintable character at the start of the functions.php which was causing errors.
### 1.1.5 ###
- Remove the header widget area and the faculty pages sidebar area, as these are no longer used in the theme.
### 1.1.4 ###
- Add a sanitize_callback to validate entries for the color scheme. This only theoretically improves security, as the color scheme values can not be directly modified in the Customizer.
### 1.1.3 ###
- Updaterd College People Tols to 0.5.7, now in master branch, including the GitHub Updater update string in the comment header of the main plugin file, so it will update through the normal WP update process from bitbucket.
### 1.1.2 ###
- Update the version of College People Tools to 0.5.6 (current version in dev branch).
### 1.1.1 ###
- Change the setting in tgmpa-config.php to not force activation of WFU LDAP Authentication, or else the plugin is difficult to replace with a different authentication solution, if someone wants to use another plugin.
### 1.1###
- Bundled the WFU LDAP Authenticaion plugin by Jeff Muday into the theme, available for install through the TGM Plugin Activator functionality.
### 1.0.2 ###
- Remove the notices about recommended plugins.
### 1.0.1 ###
- Rewrite of this Readme.md file to reflect the true nature of this new theme. (This theme was forked from College Web Starter, also created by Robert Vidrine. There are still some areas in the theme which refer to College Web Starter, which will be cleaned up in subsequent versions.) 
### 1.0 ###
- Official launch version. Contains all the features deemed necessary for most academic departments and programs, and several plugin suggestions.
Changelog:
### 1.2.1 ###
- Updated the bundled version of College People Tools to 0.6, which includes changes to allow one-click updating (along with the normal WordPress update process). Also changed the way that Font Awesome icon fonts were included, bundling them with the theme instead of calling them from a CDN (which is discouraged by the WordPress theme review team and also gets graded down in Google Page Speed Insights).
### 1.2 ###
- Added 'Department Contact Information' section to the customizer which modifies the default footer content, allowing one to simply input their department or program's contact information (mailing address, office location, phone, fax and email) and have this information appear in a formatted fashion in the footer, instead of having to build this from scratch. If one instead puts one or more widgets into the footer area, the default footer content will disappear and be replaced by their widgets.
### 1.1.6 ###
- Remove the unprintable character at the start of the functions.php which was causing errors.
### 1.1.5 ###
- Remove the header widget area and the faculty pages sidebar area, as these are no longer used in the theme.
### 1.1.4	###
- Add a sanitize_callback to validate entries for ### the color scheme. This only theoretically improves security, as the color scheme values can not be directly modified in the Customizer.
### 1.1.3	###
- Updaterd College People Tols to 0.5.7, now in master branch, including the GitHub Updater update string in the comment header of the main plugin file, so it will update through the normal WP update process from bitbucket.
### 1.1.2	###
- Update the version of College People Tools to 0.5.6 (current version in dev branch).
### 1.1.1	###
- Change the setting in tgmpa-config.php to not force activation of WFU LDAP Authentication, or else the plugin is difficult to replace with a different authentication solution, if someone wants to use another plugin.
### 1.1 ###
- Bundled the WFU LDAP Authenticaion plugin by Jeff Muday into the theme, available for install through the TGM Plugin Activator functionality.
### 1.0.2	###
- Remove the notices about recommended plugins.
### 1.0.1	###
- Rewrite of this Readme.md file to reflect the true nature of this new theme. (This theme was forked from College Web Starter, also created by Robert Vidrine. There are still some areas in the theme which refer to College Web Starter, which will be cleaned up in subsequent versions.) 
### 1.0 ###
- Official launch version. Contains all the features deemed necessary for most academic departments and programs, and several plugin suggestions.
