<?php
/**
 * The footer sidebar
 *
 */

if ( ! is_active_sidebar( 'sidebar-footer' ) ) { ?>
<div id="supplementary">
	<div id="footer-widgets" class="footer-widgets widget-area clear default-footer-content" role="complementary">
		<?php 
		$args = array(
					'before_widget' => '<aside class="widget %1$s">',
					'after_widget'  => '</aside>',
					'before_title'  => '<h2 class="widget-title">',
					'after_title'   => '</h2>'
		);
		the_widget( 'Dept_Contact_Info_Widget', 'title=' . get_bloginfo( 'title' ), $args ); ?>
		<aside id="campuslinks" class="widget widget_text">
			<h2 class="widget-title">Campus Links</h2>
			<div class="textwidget">
				<div>
					<p>
						<a href="http://college.wfu.edu/">WFU College</a>
					</p>
					<p>
						<a href="http://zsr.wfu.edu">ZSR Library</a>
					</p>
					<p>
						<a href="http://www.wfu.edu/academics/calendars/">
							Academic Calendar
						</a>
					</p>
					<p>
						<a href="https://win.wfu.edu/">WIN</a>
					</p>
				</div>
			</div>
		</aside>
	</div><!-- #footer-sidebar -->
</div><!-- #supplementary -->

		<?php }
else { ?>

<div id="supplementary">
	<div id="footer-widgets" class="footer-widgets widget-area clear" role="complementary">
		<?php dynamic_sidebar( 'sidebar-footer' ); ?>
	</div><!-- #footer-sidebar -->
</div><!-- #supplementary -->
<?php }